#!/bin/bash
# __variables
_user="$(id -u -n)"
DIR="/home/$_user/lab/myLog"
_cd=$(date +"%Y-%m-%d")
_log="$DIR/$_cd-creationClient.log"
_err="ERROR ::"

echo -e "Wanna create a new User for Ubuntu ??\rType is username to continue.\n"
read _username
if [ "$_username" ]; then
        # Proc to create user and folders
        $(sudo useradd -m -p $(echo $_username | openssl passwd -1 -stdin) $_username)
else
        echo "$_err you don't give a good entry for username"
        exit 1
fi
_userExist="$(getent passwd | awk -F: '{ print $1 }' | grep $_username)"
if [ $_userExist ]; then
        _directories="/home/$_username/{www/{html,images,css},sources}}/"
        if [ -d "$_directories" ];then
                echo "$_err The New User Folder already Exists !!!!"
                exit 1
        else
                #$(sudo mkdir -p "$_directories")
                $(sudo mkdir -p /home/$_username/www/{images,html,css})
                $(sudo mkdir -p /home/$_username/sources/)
        fi
        if [ -d "$DIR" ];then
                echo "$_err The Log Folder already exists !!!!"
                exit 1
        else
                $(mkdir "$DIR")
        fi
        if [ -f "$_log" ];then
                echo "$_err log file already exists !!!!"
                exit 1
        else
                $(touch "$DIR/$_cd-creationClient.log")
        fi
        echo "L'Utilisateur $_user a créer l'utilisateur $_username le $_cd." >> $_log
        $(cat "$DIR/$_cd-creationClient.log")
else
        echo "$_err no user existing"
fi

